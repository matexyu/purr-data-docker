FROM ubuntu:16.04

USER 0

# -------------------------------------------------------------------
# Install basic utilities
# -------------------------------------------------------------------
RUN apt -qq -y update && apt -qq -y install \
  man \
  wget \
  unzip

# -------------------------------------------------------------------
# Install critical dependencies
# -------------------------------------------------------------------
RUN apt -qq -y update && apt -qq -y install \
  libnss3-dev \
  libcups2 \
  libxcomposite1 \
  libxcursor1 \
  libxi6 \
  libxtst6 \
  libfontconfig1 \
  libgconf-2-4 \
  libpangocairo-1.0-0 \
  libxss1 \
  libxrandr2 \
  libatk-bridge2.0-0 \
  libgtk2.0-0

# -------------------------------------------------------------------
# Download Purr Data
# -------------------------------------------------------------------
ARG PURR_DATA_VERSION="2.9.0"
ARG PURR_DATA_ARCHIVE_NAME="pd-l2ork-${PURR_DATA_VERSION}-ubuntu_16.04-x86_64"

RUN wget -q -nv -O purr-data.zip \
  https://github.com/agraef/purr-data/releases/download/${PURR_DATA_VERSION}/${PURR_DATA_ARCHIVE_NAME}.zip \
  >/dev/null 2>/dev/null \
  && unzip purr-data.zip \
  && rm purr-data.zip

RUN DEBIAN_FRONTEND='noninteractive' apt -qq -y install --no-install-recommends -f $(find . -name "pd-l2ork*.deb" -type f)

# -------------------------------------------------------------------
# Install some (non-critical) run-time dependencies
# -------------------------------------------------------------------
RUN DEBIAN_FRONTEND='noninteractive' apt -qq -y update && apt -qq -y install \
  libcanberra-gtk-module \
  libmagick++-6.q16-5v5  \
  libdc1394-22  \
  libv4l2rds0 \
  libmpeg3-2 \
  libgmerlin-avdec1

# -------------------------------------------------------------------
# Enable SSH access
# -------------------------------------------------------------------
RUN DEBIAN_FRONTEND='noninteractive' apt -qq -y update && apt -y -qq install --no-install-recommends \
    openssh-server && \
    mkdir -p /var/run/sshd

COPY addNonRootUser.sh  /

CMD /usr/sbin/sshd && /bin/bash
