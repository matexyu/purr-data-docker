#!/usr/bin/env bash

set -eu

imageName="purr-data-docker-image"
containerName="purr-data-docker"

nonRootUserId=$(id -u)
nonRootUserName="ubuntu"
nonRootUserPassword="ubuntu"

function main() {
  buildImage
  runDetachedContainer
  addNonRootUser
  printFinalTips
}

function buildImage() {
  echo "Building Docker image '${imageName}' ..."
  time docker build -t ${imageName} .
}

function runDetachedContainer() {
  echo "Starting container '${containerName}' in detached mode ..."
  # https://stackoverflow.com/a/28395350
  # Very useful resources: 
  # - https://github.com/oriaks/docker-scenic
  xhost +

  docker run -tid --privileged \
    -p 0:22 \
    -p 10000:10000/udp \
    -p 12000:12000/udp \
    -e DISPLAY="${DISPLAY}" \
    -v "/tmp/.X11-unix:/tmp/.X11-unix:ro" \
    -v "/dev/snd:/dev/snd:rw" \
    -v "/dev/video0:/dev/video0:rw" \
    -v "/run/user/$(id -u)/pulse:/run/pulse:rw" \
    -v "/tmp/.X11-unix:/tmp/.X11-unix:ro" \
    -v "/var/run/dbus/system_bus_socket:/var/run/dbus/system_bus_socket:rw" \
    -v ${HOME}:/home/ubuntu \
    --name ${containerName} \
    ${imageName}
}

function addNonRootUser() {
  docker exec --user root ${containerName} /addNonRootUser.sh ${nonRootUserId} ${nonRootUserName} ${nonRootUserPassword}
}

function getContainerSshPortNumber() {
  docker port ${containerName} | grep '^22/tcp' | cut -d':' -f2
}

function printFinalTips() {
  echo
  echo "=========================================================================="
  echo "Final Tips:"
  echo
  echo "To access via SSH, say:  ssh -p $(getContainerSshPortNumber) ${nonRootUserName}@localhost  (password: ${nonRootUserPassword})"
  echo
  echo "List of exposed TCP / UDP ports:"
  echo
  docker port ${containerName}
  echo
  echo "You can launch Purr Data using the script: ./launchPurrDataOverSsh.sh"
  echo
  echo "Low-level Usage:"
  echo
  echo "To attach to the container, say:  docker attach ${containerName}"
  echo "When in the container terminal:"
  echo "1. switch to the non-root user:  su ${nonRootUserName}"
  echo "2. execute the command:  pd-l2ork"
  echo
  echo "=========================================================================="
  echo
}


main $@
