#!/usr/bin/env bash

set -eu

if [[ $# -ne 3 ]] ; then
  echo "Usage: `basename $0` <nonRootUserId>  <nonRootUserName>  <nonRootUserPassword>"
  exit 1
fi

nonRootUserId="$1"
nonRootUserName="$2"
nonRootUserPassword="$3"

echo "Adding user '${nonRootUserName}' with id ${nonRootUserId} ..."

useradd \
  --create-home \
  --shell /bin/bash \
  --uid ${nonRootUserId} \
  ${nonRootUserName}

echo "Setting password: ${nonRootUserPassword} ..."
echo "${nonRootUserName}:${nonRootUserPassword}" | chpasswd

echo " ... user with password added successfully."
echo
