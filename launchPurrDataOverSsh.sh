#!/usr/bin/env bash

set -eu

if ! command -v sshpass > /dev/null ; then
    echo "ERROR: 'sshpass' not found, install it with this command: sudo apt install sshpass"
    exit 1
fi

containerSshPort="$(docker port purr-data-docker | grep '^22/tcp' | cut -d':' -f2)"

echo "Invoking pd-l2ork over SSH, port ${containerSshPort} ... "

sshpass -v -p 'ubuntu' ssh -o "StrictHostKeyChecking no" -XY -p ${containerSshPort} ubuntu@localhost DISPLAY=':0' pd-l2ork
