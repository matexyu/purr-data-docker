# purr-data-docker

Run Purr Data on Ubuntu 18.04 by installing it in a Docker Conatainer running 
an older Ubuntu version, 16.04, and forwarding all needed audio / video devices 
via Docker volume and file bind-mounts.


Purr Data:
* https://agraef.github.io/purr-data/
* https://github.com/agraef/purr-data

 
# Steps

1. First, install Docker on your machine.
    * For example, follow these steps: https://github.com/docker/docker-install
2. Check that the Purr data version specified in the `Dockerfile` is ok for you. The Purr Data version is specified as a docker Argument.
    * To install a newer / different Purr Data version, check the available releases here: https://github.com/agraef/purr-data/releases
3. Call the `./startPurrDataContainer.sh` script. This script performs the following tasks:
    * the Docker Image is first built locally.
    * an user named `ubuntu` is created with your same User ID (so you are "yourself" inside the Docker Container). Its password is `ubuntu`.
    * your `${HOME}` folder will be mounted right in the Container's `ubuntu` user's home (`/home/ubuntu`).
    * a new Container called `purr-data-docker` will be started and left running in detached ("background") mode.


# Launching Purr Data

Once the container is running, there are two ways you can launch Purr Data.

The easiest way is to use the provided script for launching Purr Data over SSH with X forwarding.


## Launching Over SSH (Using X Forwarding)

Just call the `launchPurrDataOverSsh.sh` script and you should be good to go.


## From inside the Docker Container

Follow these steps:
1. attach to the container: `docker attach purr-data-docker`
2. once attached, from the root terminal, type `su ubuntu` to switch to the `ubuntu` user (like your host machine user, but inside the container).
3. type `p2-l2ork` in the terminal (or if you want, `p2-l2ork &` to launch in background)
